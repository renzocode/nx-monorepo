# NxMonorepo

## Getting monorepo project

```bash
$ npx --ignore-existing create-nx-workspace nx-monorepo --preset=angular
```

note:
  ng serve show a problem.

"Error from chokidar (/home/..): Error: ENOSPC: System limit for number of file watchers reached, watch "

```bash
$ export CHOKIDAR_USEPOLLING=1
```
Generate home component.
```bash
$ ng generate component home --project=finance
```

Generate home module
```bash
$ ng generate module home --project=finance
```

Generate home routing
```bash
$ ng generate module app-routing --flat --module=app
```

